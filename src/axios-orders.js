import axios from 'axios';

const instance = axios.create({
    baseURL:'https://react-my-burger-28f68.firebaseio.com/'
});

export default instance;